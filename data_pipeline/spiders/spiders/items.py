"""
Models used in scrapy spiders
"""
from dataclasses import dataclass
import json
from abc import ABC
from bs4 import BeautifulSoup


@dataclass
class UrlInfo():
    """
    Data class to handle url info
    """
    link: str
    category: str
    article_id: str


class Article(ABC):
    """
    Abstract class to handle BBC articles
    """

    def __init__(self, _type, response) -> None:
        self._type = _type
        self.response = response
        self.soup = BeautifulSoup(response.body, "html.parser")

    @property
    def url_info(self):
        """
        Add information contained in the url
        """
        url = self.response.request.url
        category, article_id = url.split('/')[-2:]
        return UrlInfo(link=url, category=category, article_id=article_id)

    @property
    def content(self):
        """
        Article content property
        """
        ...

    @property
    def date(self):
        """
        Date property
        """
        ...

    @property
    def title(self):
        """
        Title property
        """
        ...

    def get_fields(self):
        """
        Get all fields
        """
        return {
            **self.url_info.__dict__,
            **{
                'type': self._type,
                'title': self.title,
                'date': self.date,
                'content': self.content
            }
        }


class TextArticle(Article):
    """
    Text Article
    """

    def __init__(self, response) -> None:
        super().__init__(_type='article', response=response)

        self.article_header = self.get_header_from_soup()

    @property
    def content(self):
        """
        Article content property
        """
        return "\n".join(
            [
                p.get_text()
                for p
                in self.soup.find(
                    'div', {"class": ["qa-story-body", "story-body"]}
                ).find_all('p')
            ]
        )

    @property
    def date(self):
        """
        Date property
        """
        if self.article_header:
            return self.article_header.find('time').attrs['datetime']
        return None

    @property
    def title(self):
        """
        Title property
        """
        if self.article_header:
            return self.article_header.find('h1').get_text()
        return None

    def get_header_from_soup(self):
        """
        Get header from soup
        """
        return next(iter([
            header
            for header in self.soup.find_all('header')
            if header.has_attr('data-reactid')
        ]), None)


class VideoArticle(Article):
    """
    Video Article
    """

    def __init__(self, response) -> None:
        super().__init__(_type='video', response=response)

    @property
    def content(self):
        """
        Article content property
        """
        def get_content():
            for div in self.soup.find_all('div'):
                if any(['StyledSummary' in _class for _class in div.attrs.get('class', [])]):
                    return div
        return get_content().get_text()

    @property
    def date(self):
        """
        Date property
        """
        return self.soup.find('article').find('time').attrs['datetime']

    @property
    def title(self):
        """
        Title property
        """
        return self.soup.find('article').find('h1').get_text()


class LiveArticle():
    """
    Live Article
    """
    COLS_TO_DELETE = ['publisher', '@context', '@type']
    COLS_TO_DELETE_FROM_UPDATES = COLS_TO_DELETE + ['author', 'image']

    def __init__(self, response) -> None:
        """
        Extract live articles
        """
        soup = BeautifulSoup(response.body, "html.parser")
        json_info = self.get_json_ld(soup)
        updates = json_info.pop('liveBlogUpdate')
        url_info = self.get_url_info(response)

        # cleaning
        json_info = self.clean_main(json_info)
        self.updates = self.clean_updates(updates)

        self.updates = [
            {
                **update,
                **{
                    'type': 'live',
                    'main_article_info': json_info,
                },
                **url_info.__dict__
            }
            for update in self.updates
        ]

    def get_json_ld(self, soup):
        """
        Get json ld from the soup
        """
        return json.loads(soup.find('script', {'type': 'application/ld+json'}).get_text())

    def clean_main(self, json_info):
        """
        Clean main article
        """
        for to_del in self.COLS_TO_DELETE:
            del json_info[to_del]
        return json_info

    def clean_updates(self, updates):
        """
        Clean every article from updates
        """
        for update in updates:
            for to_del in self.COLS_TO_DELETE_FROM_UPDATES:
                del update[to_del]
        return updates

    def get_url_info(self, response):
        """
        Add information contained in the url
        """
        url = response.request.url
        category, article_id = url.split('/')[-2:]
        return UrlInfo(link=url, category=category, article_id=article_id)

    @property
    def all_updates(self):
        """
        Return list of updates
        """
        return self.updates
