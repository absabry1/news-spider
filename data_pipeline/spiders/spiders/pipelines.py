"""
Pipelines to execute when we get info from the spider
"""

import json
from urllib.parse import quote_plus
import pymongo
from itemadapter import ItemAdapter
from scrapy.exceptions import DropItem
from settings.settings import (
    MONGO_DB_CONNECT, MONGO_DB_CREDENTIALS,
    MONGO_INITDB_DATABASE, BATCH_SIZE, FROM_DOCKER
)


class JsonWriterPipeline:
    """
    Write json to file
    """

    def __init__(self) -> None:
        self.file = None

    def open_spider(self, spider):
        """
        To execute when we open the spider
        """
        spider.logger.info("Spider is opened, we will open the file")
        self.file = open(f"./logs/{spider.name}.json", 'w', encoding='UTF-8')

    def close_spider(self, spider):
        """
        To execute when we close the spider
        """
        self.file.close()
        spider.logger.info("Spider is closed, we will close the file")

    def process_item(self, item, spider):
        """
        Process each item
        """
        spider.logger.info("Item handled and updated in the JSON file")
        line = json.dumps(ItemAdapter(item).asdict()) + "\n"
        self.file.write(line)
        return item


class DuplicatesPipeline:
    """
    Handle duplicates data before any output
    """

    def __init__(self) -> None:
        self.ids_seen = set()

    def process_item(self, item, spider):
        """
        Process each item
        """
        if item['id'] in self.ids_seen:
            spider.logger.error(f"Item {item} has already been treated.")
            raise DropItem(f"Repeated items found: {item}")

        self.ids_seen.add(item['text'])
        return item


class MongoPipeline:
    """
    Mongodb shipper
    """

    def __init__(self) -> None:
        # TODO Pas beau du tout... avoir comment le donner de app.py...
        host = 'mongodb' if FROM_DOCKER else MONGO_DB_CONNECT['HOST']
        self.uri_connection = f"mongodb://{quote_plus(MONGO_DB_CREDENTIALS['USERNAME'])}:"\
            f"{quote_plus(MONGO_DB_CREDENTIALS['PASSWORD'])}@"\
            f"{host}:{MONGO_DB_CONNECT['PORT']}"
        self.client = None
        self.database = None
        self.data = []

    def open_spider(self, spider) -> None:
        """
        Will be executed when spider is open
        """
        spider.logger.info("Spider is opened, we will connect to MongoDB")
        self.client = pymongo.MongoClient(self.uri_connection)
        self.database = self.client[MONGO_INITDB_DATABASE]
        spider.logger.info("Connected...")

    def close_spider(self, spider) -> None:
        """
        Will be executed when spider is closed
        """
        if self.data:
            self.bulk_data(spider)
        spider.logger.info("Spider is closed. Abort connection...")
        self.client.close()
        spider.logger.info("Disconnected...")

    def process_item(self, item, spider):
        """
        Will be executed to process each item
        """
        spider.logger.info("Handle Item...")
        self.data.append(ItemAdapter(item).asdict())
        if len(self.data) >= BATCH_SIZE:
            result = self.bulk_data(spider)
            if not result:
                spider.logger.error("Possible data loss in this batch!!")
        return item

    def bulk_data(self, spider) -> bool:
        """
        Bulkd data to mongodb instead of inserting one by one.
        Return bulk to verify that every document has been pushed correctly
        """
        docs_count = len(self.data)
        spider.logger.info(
            f"Bulking {docs_count} documents in {spider.name}_col")
        ids = self.database[f"{spider.name}_col"].insert_many(self.data)
        result = len(ids.inserted_ids) == docs_count
        if not result:
            spider.logger.error("Possible data loss in this batch!!")
            spider.logger.error(f"Had {docs_count}, only {len(ids)} succeded")

        self.data = []  # reset data
        return result
