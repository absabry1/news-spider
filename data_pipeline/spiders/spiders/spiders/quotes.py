"""
Tutorial to learn how to use scrapy
in a real world example
"""
# pylint: disable=arguments-differ
#from scrapy.loader import ItemLoader
#from ..items import QuotesItem
import scrapy


class QuotesSpider(scrapy.Spider):
    """
    Quotes spider
    """
    name = "quotes"

    start_urls = ['http://quotes.toscrape.com/']

    custom_settings = {
        'LOG_FILE': f"./logs/{name}.log",
    }

    def parse(self, response):
        for quote in response.css('div.quote'):
            # TODO make it work, to have a hash in the data class
            # https://www.youtube.com/watch?v=wyE4oDxScfE&list=PLRzwgpycm-Fjvdf7RpmxnPMyJ80RecJjv&index=4
            # it = ItemLoader(item=QuotesItem, selector=quote)
            # it.add_css('text', 'span.text::text')
            # it.add_css('author', 'small.author::text')
            # yield it.load_item()
            yield {
                '_type': 'quote',
                'text': quote.css('span.text::text').get(),
                'author': quote.css('small.author::text').get(),
            }

        author_page_links = response.css('.author + a')
        yield from response.follow_all(author_page_links, self.parse_author)

        next_page = response.css('li.next a::attr(href)').get()
        if next_page is not None:
            yield response.follow(next_page, self.parse)

    def parse_author(self, response):
        """
        Add information about author
        """
        def extract_with_css(query):
            return response.css(query).get(default='').strip()

        yield {
            'type': 'author',
            'name': extract_with_css('h3.author-title::text'),
            'birthdate': extract_with_css('.author-born-date::text'),
            'bio': extract_with_css('.author-description::text'),
        }
