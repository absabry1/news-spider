"""
Crawl spider example
"""

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from bs4 import BeautifulSoup
from settings.settings import SPORTS
from ..items import *


class BBCCrawler(CrawlSpider):
    """
    Crawl and listen to books scrapper
    """

    name = 'bbc_crawl'

    # custom_settings = {
    #     'LOG_FILE': f"./logs/{name}.log",
    # }

    ## TODO handle duplicates if not existing yet
    
    allowed_domains = ['www.bbc.com']
    start_urls = [
        'https://www.bbc.com/sport'
    ]

    rules = (
        # list of all top sports (defined sports)
        Rule(LinkExtractor(
            restrict_xpaths='//*[@id="sp-nav-flyout"]/div[1]/ul[2]/li/a',
        ), follow=True),
        Rule(LinkExtractor(
            allow=(r'(' + '|'.join(SPORTS) + ')/\d+', )
        ), callback='parse_item'),
    )

    def parse_item(self, response):
        """
        Parse one item from the books
        """
        soup = BeautifulSoup(response.body, "html.parser")

        is_video = any(
            ['StyledMediaItem' in _class for _class in soup.find(
                'article').attrs.get('class', [])]
        )
        is_live = 'live' in response.request.url.split('/')

        if is_live:
            article = LiveArticle(response)
            for article in article.all_updates:
                yield article
        elif is_video:
            article = VideoArticle(response)
            yield article.get_fields()
        else:
            article = TextArticle(response)
            yield article.get_fields()
