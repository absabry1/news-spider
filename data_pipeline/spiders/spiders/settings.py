"""
BOT settings
"""

BOT_NAME = 'spiders'

SPIDER_MODULES = ['spiders.spiders']
NEWSPIDER_MODULE = 'spiders.spiders'

ROBOTSTXT_OBEY = True

ITEM_PIPELINES = {
   'spiders.pipelines.JsonWriterPipeline': 300,
}
