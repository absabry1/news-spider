"""
Default settings that are included in the docker file.
If you change this option, think about changing them in the Dockerfile
and docker-compose too.
"""
# MONGODB CONFIG
MONGO_DB_CONNECT = dict(
    PORT="27017",
    HOST="localhost"
)
MONGO_DB_CREDENTIALS = dict(
    USERNAME="newsroot",
    PASSWORD="Wp6S9?AFzA"
)
MONGO_INITDB_DATABASE = "news"

# MONGODB BATCH SIZE
BATCH_SIZE = 500

# list of availble sports
# https://www.bbc.com/sport
# and on top-right : 'All sports' button
SPORTS = ['basketball', 'football']

FROM_DOCKER = True # to use mongosb service name instead of localhost
