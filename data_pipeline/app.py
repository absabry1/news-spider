"""
Test readiblity lib
"""
import os

from scrapy.crawler import CrawlerProcess
from spiders.spiders.spiders.bbc_crawler import BBCCrawler
from spiders.spiders.pipelines import MongoPipeline
from scrapy.crawler import CrawlerRunner
from twisted.internet import reactor
from twisted.internet import task


if __name__ == '__main__':
    TIMEOUT = 60 * 60  # run every hour

    print("========================START PROCESS========================")
    print(f"Current workdir {os.getcwd()}")
    print(f"Running every {TIMEOUT} seconds")

    process = CrawlerProcess(settings={
        'ITEM_PIPELINES': {
            # JsonWriterPipeline: 300,
            # DuplicatesPipeline:300,
            MongoPipeline: 500,
        }
    })
    print("========================AFTER PROCESS========================")

    def run_spider():
        """
        Run crawler spider
        """
        print("========================IN RUN SPIDER========================")
        l.stop()
        runner = CrawlerRunner(settings={
            'ITEM_PIPELINES': {
                # JsonWriterPipeline: 300,
                # DuplicatesPipeline:300,
                MongoPipeline: 500,
            }
        })
        d = runner.crawl(BBCCrawler)
        d.addBoth(lambda _: l.start(TIMEOUT, False))
        print(
            f"========================WAITIG FOR {TIMEOUT} SECONDS========================")

    l = task.LoopingCall(run_spider)
    l.start(TIMEOUT)

    reactor.run()
