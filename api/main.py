"""
Main entry to fastAPI
"""

from fastapi import FastAPI
from mongo_db import MongoData

app = FastAPI()
mongo_client = MongoData(
    host="mongodb"  # run from docxker using service name
)


@app.get("/")
def read_root():
    """
    Home
    """
    return {"Message": "Follow http://localhost:2020/docs to visualize all the endpoints"}


@app.get("/items/category/{_type}")
def read_category(_type: str):
    """
    Get items having type of article:category
    """
    result = {
        "query": {
            "category": _type
        },
        "data": [],
        "success": False,
        "found": False,
        "items": 0
    }
    try:
        items = mongo_client.get_by_query({'type': _type})
        result["data"] = items
        result["success"] = True
        result["found"] = True
        result["items"] = len(items)
    except Exception as ex:
        result["error"] = ex
    return result


@app.get("/items/id/{item_id}")
def read_article_id(item_id: int):
    """
    Get items having id:item_id
    """
    result = {
        "query": {
            "item_id": item_id
        },
        "data": [],
        "success": False,
        "items": 0
    }
    try:
        items = mongo_client.get_by_query({'article_id': str(item_id)})
        result["data"] = items
        result["success"] = True
        result["items"] = len(items)
    except Exception as ex:
        result["error"] = ex
    return result


@app.get("/items/search/{search_text}")
def search_by_keywords(search_text: str):
    """
    Search in the database for {search_text}
    """
    result = {
        "query": {
            "search_phrase": search_text
        },
        "data": [],
        "success": False,
        "items": 0
    }
    try:
        items = mongo_client.get_by_text(search_text)
        result.update({
            "data": items,
            "success": True,
            "items": len(items)
        })
    except Exception as ex:
        result["error"] = ex
    return result
