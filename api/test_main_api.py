"""
Unit test for API
"""
from urllib.parse import quote_plus
from fastapi.testclient import TestClient
from main import app
from mongo_db import MongoData

client = TestClient(app)

mongo_client = MongoData()


def test_home_page():
    """
    Test home page
    """
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {
        "Message": "Follow http://localhost:2020/docs to visualize all the endpoints"}


def test_category_search():
    """
    Test category API endpoint
    """
    response = client.get("/items/category/live")
    assert response.status_code == 200
    res = response.json()
    for item in ["query", "data", "success", "items"]:
        assert item in res

    items = mongo_client.get_by_query({'type': 'live'})
    assert res["items"] == len(items)
    if items:
        assert res["items"] > 0
        assert res["success"] is True
        assert next(iter(res["data"]), {}).get("type") == "live"


def test_keyword_search():
    """
    Test search by keywords API endpoint
    """
    items = mongo_client.get_by_query({'type': 'article'})
    item = None
    if items:
        item = next(iter(items))
    title = item.get("title")

    response = client.get(f"/items/search/{quote_plus(title)}")
    assert response.status_code == 200
    res = response.json()
    for item in ["query", "data", "success", "items"]:
        assert item in res

    if res["items"]:
        assert res["data"][0]["title"] == title
