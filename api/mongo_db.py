"""
Get data from mongodb database
"""

from urllib.parse import quote_plus

import pymongo
from settings.settings import (
    MONGO_INITDB_DATABASE, MONGO_DB_CONNECT,
    MONGO_DB_CREDENTIALS
)


class MongoData():
    """
    Mongo db client to get the data
    """

    def __init__(self, host=MONGO_DB_CONNECT['HOST']) -> None:
        """
        Initialize mongodb client
        """
        self.client = pymongo.MongoClient(
            f"mongodb://{quote_plus(MONGO_DB_CREDENTIALS['USERNAME'])}:"
            f"{quote_plus(MONGO_DB_CREDENTIALS['PASSWORD'])}@"
            f"{host}:{MONGO_DB_CONNECT['PORT']}"
        )
        self.collection = self.client[MONGO_INITDB_DATABASE]['bbc_crawl_col']

        # create text index
        self.collection.create_index([
            ("title", pymongo.TEXT),
            ("content", pymongo.TEXT),
            ("articleBody", pymongo.TEXT),
        ])

    def get_by_query(self, query: dict):
        """
        Get data having current category
        """
        cursor = self.collection.find(query, {'_id': 0})
        return [document for document in cursor]

    def get_by_text(self, text: str):
        """
        search by text
        """
        cursor = self.collection.find(
            {"$text": {"$search": text}},
            {'score': {'$meta': "textScore"}},
        ).sort([('score', {'$meta': 'textScore'})]).limit(100)
        documents = [document for document in cursor]
        # TODO remove _id using the query directly
        for document in documents:
            del document["_id"]
        return documents
