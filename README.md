# News-spider

> Create a solution that crawls for articles from a news website, cleanses the response, stores in a mongo database then makes it available to search via an API

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* Docker
* Docker-compose

### Installing 

```bash
cd news-spider
docker-compose up -d
```

Visit `http://localhost:2020`

### Project Structure

```
             +-------------------+
             |                   |
             |       MAIN        |
             |                   |
             |    News-spider    |
             |                   |
             +--+-------------+--+
                |             |
                |             |
                |             |
                v             v
+---------------+-+         +-+----------------+
|                 |         |                  |
|       API       |         |  DATA-PIPELINE   |
|                 |         |                  |
| Expose results  |         |  Build & extract |
|    using API    |         |  data efficently |
+-----------------+         +------------------+
```
## Debugging 
When you debug on your local machines AND you're working with vscode, you'll need to specify:
> "python.envFile": "${workspaceFolder}/.py_env",     

in your `.vscode/.settings.json`  file.

Then, in .py_env file, you should change the folder you're working on {data_pipeline} or {api}, along with the path of your project in {WORKSPACE_FOLDER}

## License

This project is licensed under the GNU AGPLv3.

## Next steps
Next steps to improve the application or fix issues
#### Mandatory improvements
- Augmenter le taux de tests
- Gerer les duplicates dans la base pour ne pas aller les récupérer une autre fois (sauf s’il y a eu un update)

#### Data pipeline improvements
- Enrichir les données avec les meta des pages
- Enrichir les données avec des techniques innovantes de text mining comme Apriori et Readability
- Ajouter d’autres type de pages (live, text article, etc. )
- Ajouter d’autres sites comme : https://www.theguardian.com/uk/sport
- Explore/Use Strategy Pattern to innitalize data items 

#### API improvements
- Add more explicit return codes in the API
- Add more designed response with HTTP response
- Add a secure token to use the API
- Ajouter une classification de categories 
#### Production related improvements
- Use swarm/kubernets orchestrator?
- Mettre en place un .giitlab-ci-cd avec herouku pour mettre en prod 

### Nice to have
- Add ElasticSearch shipper to have a read-to-use Kibana
- Add some interactive data-viz using Plotly

## Schema registry 
1. Documentation for the organization
2. Migrations and schema evolution (do not need to change the data stored before) Backward compatible schemas
3. Schema evolution compatibility with older versions
4. Processing efficiency : Don’t need to store the field names in the events
5. Resilient : Data streams subscribers should be confident that a producer upstream won’t send them data they can’t handle
